import Vue from 'vue'
import vuetify from './plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import { firestorePlugin } from 'vuefire'

import App from './App.vue';

Vue.config.productionTip = false
Vue.use(firestorePlugin)

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
