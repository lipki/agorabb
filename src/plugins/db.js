import firebase from 'firebase/app'
import 'firebase/firestore'

// Get a Firestore instance
export const db = firebase
  .initializeApp({ 
    apiKey: "AIzaSyBOL_ttgUQ5ZBASwTWeCaffQ-yvuuOYQH4",
    authDomain: "agorabb2-ebc48.firebaseapp.com",
    databaseURL: "https://agorabb2-ebc48.firebaseio.com",
    projectId: "agorabb2-ebc48",
    storageBucket: "agorabb2-ebc48.appspot.com",
    messagingSenderId: "787147852589",
    appId: "1:787147852589:web:d9b4e88a6faf7908a3e1a4"
  })
  .firestore()

// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
const { Timestamp, GeoPoint } = firebase.firestore
export { Timestamp, GeoPoint }

// if using Firebase JS SDK < 5.8.0
db.settings({ timestampsInSnapshots: true })